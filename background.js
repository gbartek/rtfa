var sites = [
    { name: "facebook", time: 0 },
    { name: "wykop", time: 0 },
    { name: "anonimowe", time: 0 }

]

var INTERVAL = 1000;
// var MAXIMUM_TIME_SPENT = 15 * 60 * 1000;
var MAXIMUM_TIME_SPENT = 4 * INTERVAL;

function showNotification(msg) {
    console.log(msg);
    chrome.notifications.create("123", {
        type: 'basic',
        title: 'tytul',
        message: msg,
        iconUrl: chrome.runtime.getURL('icon-48.png'),
    });
}

function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

async function check() {

    while (true) {
        await sleep(INTERVAL);

        chrome.tabs.getSelected(null, function (tab) {
            updateSitesTime(tab.url);

            for (var i = 0; i < sites.length; i++) {
                if (tab.url.includes(sites[i].name)) {
                    if (sites[i].time > MAXIMUM_TIME_SPENT) {
                        var newURL = chrome.runtime.getURL('index.html');
                        chrome.tabs.create({ url: newURL })                       
                    }
                    break;
                }
            }
        });
    }
}

function showPopup() {

    var info = "Jestes na pozerajacej czas stronie juz od 15 minut! \
     \nMoze zamiast tego przeczytasz jakis artykul? \
    \nNp ten:"

    if (confirm(info) == true) {
        //...go to article
    } else {
        //... add another 5 minutes
    }
}


function updateSitesTime(url) {
    sites.forEach((el) => {
        if (url.includes(el.name)) {
            incrementVisitingTime(el);
        } else {
            decrementVisitingTime(el);
        }
    })

}
function decrementVisitingTime(site) {
    if (site.time > INTERVAL) {
        site.time -= INTERVAL / 4;
    }
    if (site.time < 0) {
        site.time = 0;
    }
}
function incrementVisitingTime(site) {
    site.time += INTERVAL;
    console.log("increasing time: " + site.name + "to " + site.time);
}


check();

