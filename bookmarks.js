function dumpBookmarks(query) {
    var bookmarkTreeNodes = chrome.bookmarks.getTree(function (bookmarkTreeNodes) {
        $('#bookmarks').append(dumpTreeNodes(bookmarkTreeNodes, query));
    });
}
function dumpTreeNodes(bookmarkNodes, query) {
    var list = $('<ul>');
    var i;
    for (i = 0; i < bookmarkNodes.length; i++) {
        console.log(bookmarkNodes[i].title + " : " + bookmarkNodes[i].id);
        list.append(dumpNode(bookmarkNodes[i], query));
    }
    return list;
}
function dumpNode(bookmarkNode, query) {
    if (bookmarkNode.title) {
        if (query && !bookmarkNode.children) {
            if (String(bookmarkNode.title).indexOf(query) == -1) {
                return $('<span></span>');
            }
        }
        var anchor = $('<a>');
        anchor.attr('href', bookmarkNode.url);
        anchor.text(bookmarkNode.title);
        anchor.click(function () {
            chrome.tabs.create({ url: bookmarkNode.url });
        });
        var span = $('<span>');
        span.append(anchor);
    }
    var li = $(bookmarkNode.title ? '<li>' : '<div>').append(span);
    if (bookmarkNode.children && bookmarkNode.children.length > 0) {
        li.append(dumpTreeNodes(bookmarkNode.children, query));
    }
    return li;
}
function testDump() {
    var result = 0;
    chrome.bookmarks.getTree(function (itemTree) {
        itemTree.forEach(function (item) {
            try {
                processNode(item);
            }
            catch (err) {
                result = err.id;
                console.log("         CATCH: " + err.msg + " " + result);
                printFolder(result);
                console.log("===========");
                console.log(myBookmarks);
                // result = err.id;
                return result;
            }
        });
    });
}
function processNode(node) {
    // recursively process child nodes
    if (node.children) {
        node.children.forEach(function (child) { processNode(child); });
        if (node.title == "reading") {
            console.log("       >> reading id is: " + node.id);
            throw { msg: "found", id: node.id };
        }
    }
}
var myBookmarks = [];
function printNode(node) {
    if (node.children) {
        node.children.forEach(function (child) { processNode(child); });
    }
    // print leaf nodes URLs to console
    if (node.url) {
        // console.log("   " + node.url);
        myBookmarks.push(node.url);
        var element = document.createElement("li");
        var anchor = document.createElement("a");
        anchor.setAttribute('href', node.url);
        anchor.innerHTML = node.title;
        anchor.onclick = function () {
            chrome.tabs.create({ url: node.url });
        };
        var span = document.createElement("span");
        span.appendChild(anchor);
        element.appendChild(anchor);
        console.log(element);
        document.getElementById("bookmarks").appendChild(element);
    }
}
function printFolder(id) {
    chrome.bookmarks.getChildren(id, function (itemTree) {
        itemTree.forEach(function (item) {
            printNode(item);
        });
    });
}
function start() {
    testDump();
}
document.addEventListener('DOMContentLoaded', function () {
    start();
});
